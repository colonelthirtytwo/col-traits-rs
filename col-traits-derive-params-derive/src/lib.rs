extern crate proc_macro;

use quote::quote;

#[proc_macro_derive(DeriveParams, attributes(derive_params))]
pub fn col_derive_params(ast: proc_macro::TokenStream) -> proc_macro::TokenStream {
	col_traits_derive_params_derive_impl::col_derive_params_impl(ast.into(), quote! {::col_traits})
		.unwrap_or_else(|e| e.to_compile_error())
		.into()
}
