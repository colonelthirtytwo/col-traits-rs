#!/bin/bash
set -euxo pipefail
./target/debug/col-traits-derive-params-derive-impl < col-traits-derive-params-derive-impl/src/attrs.rs > col-traits-derive-params-derive-impl/src/attrs.generated.rs
rustfmt -q col-traits-derive-params-derive-impl/src/attrs.generated.rs
