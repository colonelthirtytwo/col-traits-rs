use proc_macro2::TokenStream;
use quote::{
	quote,
	ToTokens,
};

/// Generates the body of a struct or variant destructuring pattern.
///
/// Takes a name mapping function that takes the index and field as arguments and
/// returns two values. The first value is `true` if `ref` should be added to the
/// pattern, or false if not. The second is a `ToTokens` type that is the name to
/// bind the field to (or `_` to ignore it).
///
/// For named fields, returns a pattern surrounded with `{}`. For unnamed fields, returns a pattern
/// surrounded in `()`. For unit fields, returns an empty token stream.
pub fn fields_pattern<O: ToTokens, F: FnMut(usize, &syn::Field) -> (bool, O)>(
	fields: &syn::Fields,
	mut name_mapper: F,
) -> TokenStream {
	match fields {
		syn::Fields::Named(_) => {
			let inner = fields
				.iter()
				.enumerate()
				.map(|(i, field)| {
					let field_name = field.ident.as_ref().unwrap();
					let (as_ref, out_name) = name_mapper(i, field);
					let as_ref_tok = if as_ref {
						quote! {ref}
					} else {
						quote! {}
					};

					quote! {#field_name : #as_ref_tok #out_name,}
				})
				.collect::<TokenStream>();
			quote! { {#inner} }
		}
		syn::Fields::Unnamed(_) => {
			let inner = fields
				.iter()
				.enumerate()
				.map(|(i, field)| {
					let (as_ref, out_name) = name_mapper(i, field);
					let as_ref_tok = if as_ref {
						quote! {ref}
					} else {
						quote! {}
					};

					quote! {#as_ref_tok #out_name,}
				})
				.collect::<TokenStream>();
			quote! { (#inner) }
		}
		syn::Fields::Unit => {
			quote! {}
		}
	}
}

/// Returns `{..}`, `(..)` or an empty token stream depending on the type of the passed-in fields.
pub fn fields_match_any(fields: &syn::Fields) -> TokenStream {
	match fields {
		syn::Fields::Named(_) => quote! {{..}},
		syn::Fields::Unnamed(_) => quote! {(..)},
		syn::Fields::Unit => quote! {},
	}
}

/// Checks if an iterator has zero or one results. If it has more, returns an error
/// with the second result.
pub fn check_unique<I: Iterator>(mut iter: I) -> Result<Option<I::Item>, I::Item> {
	let first = match iter.next() {
		Some(v) => v,
		None => {
			return Ok(None);
		}
	};
	if let Some(second) = iter.next() {
		Err(second)
	} else {
		Ok(Some(first))
	}
}

pub fn unique_attr<'a, Cond: Fn(&syn::Attribute) -> bool>(
	attrs: &'a [syn::Attribute],
	cond: Cond,
) -> Result<Option<&'a syn::Attribute>, syn::Error> {
	check_unique(attrs.iter().filter(|c| cond(c)))
		.map_err(|extra_attr| syn::Error::new_spanned(extra_attr, "Duplicate attribute on item"))
}

pub fn find_unique_attr_in_fields<'a, Cond: Fn(&syn::Attribute) -> bool>(
	fields: &'a syn::Fields,
	cond: Cond,
) -> Result<Option<(&'a syn::Field, &'a syn::Attribute)>, syn::Error> {
	let iter = fields
		.iter()
		.flat_map(|field| field.attrs.iter().map(move |attr| (field, attr)))
		.filter(|(_, attr)| cond(attr));
	check_unique(iter).map_err(|(_, extra_attr)| {
		syn::Error::new_spanned(extra_attr, "Attribute can appear on only one field")
	})
}

pub fn ty_generics(generics: &syn::Generics) -> TokenStream {
	let body = generics
		.params
		.iter()
		.map(|param| match param {
			syn::GenericParam::Type(param) => param.ident.to_token_stream(),
			syn::GenericParam::Lifetime(param) => param.lifetime.to_token_stream(),
			syn::GenericParam::Const(param) => param.ident.to_token_stream(),
		})
		.map(|ts| quote! {#ts,})
		.collect::<TokenStream>();
	if body.is_empty() {
		quote! {}
	} else {
		quote! {<#body>}
	}
}

#[cfg(test)]
pub(crate) fn check_parses<T: syn::parse::Parse>(ts: TokenStream) {
	println!("Generated body:\n{}", ts);
	match syn::parse2::<T>(ts) {
		Ok(_) => {}
		Err(err) => {
			panic!("Generated invalid rust code.\nError: {}", err);
		}
	}
}
