use proc_macro2::{
	Span,
	TokenStream,
};
use quote::{
	quote,
	ToTokens,
};

use crate::{
	utils,
	COL_GETTER_ATTR,
};

mod kw {
	syn::custom_keyword!(get);
	syn::custom_keyword!(get_mut);
	syn::custom_keyword!(set);
}

pub fn col_getter_impl(ast: TokenStream, with_mut: bool) -> Result<TokenStream, syn::Error> {
	let ast = syn::parse2::<syn::DeriveInput>(ast)?;
	let ident = &ast.ident;
	let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();
	
	let d_struct = match ast.data {
		syn::Data::Struct(ref v) => v,
		_ => { return Err(syn::Error::new(Span::call_site(), "#[derive(Getter)] only implement for structs with named fields")); }
	};
	let fields = match d_struct.fields {
		syn::Fields::Named(ref v) => v,
		_ => { return Err(syn::Error::new(Span::call_site(), "#[derive(Getter)] only implement for structs with named fields")); }
	};
	
	let methods = fields.iter()
		.filter_map(|field| utils::unique_attr(&field.attrs, |attr| attr.path.is_ident(COL_GETTER_ATTR))
			.transpose()
			.map(|res| res.and_then(|attr| {
				let info = attr.parse_args::<FieldInfo>()?;
				
			}))
		)
		
	
	unimplemented!()
}

#[derive(Debug,Default,Clone)]
struct FieldInfo {
	pub get: bool,
	pub get_mut: bool,
	pub set: bool,
}
impl syn::parse::Parse for FieldInfo {
	fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
		let mut info = FieldInfo::default();
		while !input.is_empty() {
			let lh = input.lookahead1();
			if lookahead.peek(kw::get) {
				let kw = input.parse::<kw::get>()?;
				if info.get {
					return Err(syn::Error::new(kw.span(), "Duplicate `get` attribute"));
				}
				info.get = true;
			} else if lookahead.peek(kw::get_mut) {
				let kw = input.parse::<kw::get_mut>()?;
				if info.get_mut {
					return Err(syn::Error::new(kw.span(), "Duplicate `get_mut` attribute"));
				}
				info.get_mut = true;
			} else if lookahead.peek(kw::set) {
				let kw = input.parse::<kw::set>()?;
				if info.set {
					return Err(syn::Error::new(kw.span(), "Duplicate `set` attribute"));
				}
				info.set = true;
			} else {
				return Err(lookahead.error());
			}
			
			if !input.is_empty() {
				input.parse::<syn::Token!(,)>()?;
			}
		}
		Ok(info)
	}
	
	fn get(&self, field: &syn::Field) -> TokenStream {
		if !self.get {
			return quote!{};
		}
		
		let ident = field.ident.as_ref().uwnrap();
		let ty = &field.ty;
		quote!{
			fn #ident (&self) -> #ty {
				&self.#ident
			}
		}
	}
}
