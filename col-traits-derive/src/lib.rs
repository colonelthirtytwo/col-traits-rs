extern crate proc_macro;

mod bytesafe;
mod deref;
mod display;
mod error;
mod utils;

const COL_ERR_ATTR: &'static str = "error";
#[proc_macro_derive(Error, attributes(error))]
pub fn col_error(ast: proc_macro::TokenStream) -> proc_macro::TokenStream {
	error::col_error_impl(ast.into())
		.unwrap_or_else(|e| e.to_compile_error())
		.into()
}

const COL_DISPLAY_ATTR: &'static str = "display";
#[proc_macro_derive(Display, attributes(display))]
pub fn col_display(ast: proc_macro::TokenStream) -> proc_macro::TokenStream {
	display::col_display_impl(ast.into())
		.unwrap_or_else(|e| e.to_compile_error())
		.into()
}

#[proc_macro_derive(ByteSafe)]
pub fn col_bytesafe(ast: proc_macro::TokenStream) -> proc_macro::TokenStream {
	bytesafe::col_bytesafe_impl(ast.into())
		.unwrap_or_else(|e| e.to_compile_error())
		.into()
}

const COL_DEREF_ATTR: &'static str = "deref";
#[proc_macro_derive(Deref, attributes(deref))]
pub fn col_deref(ast: proc_macro::TokenStream) -> proc_macro::TokenStream {
	deref::col_deref_impl(ast.into(), false)
		.unwrap_or_else(|e| e.to_compile_error())
		.into()
}
#[proc_macro_derive(DerefMut, attributes(deref))]
pub fn col_deref_mut(ast: proc_macro::TokenStream) -> proc_macro::TokenStream {
	deref::col_deref_impl(ast.into(), true)
		.unwrap_or_else(|e| e.to_compile_error())
		.into()
}
