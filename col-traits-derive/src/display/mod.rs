use proc_macro2::{
	Span,
	TokenStream,
};
use quote::quote;
use syn::parse::Parser as _;

use crate::{
	utils,
	COL_DISPLAY_ATTR,
};

pub fn col_display_impl(ast: TokenStream) -> Result<TokenStream, syn::Error> {
	let ast = syn::parse2::<syn::DeriveInput>(ast)?;
	let item_ident = &ast.ident;
	let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();
	match ast.data {
		syn::Data::Struct(ref d_struct) => {
			let destruct_body = utils::fields_pattern(&d_struct.fields, |i, field| {
				(
					true,
					field
						.ident
						.clone()
						.unwrap_or_else(|| quote::format_ident!("v{}", i)),
				)
			});

			let body =
				match DisplayMode::mode_for_variant(&ast.ident, &ast.attrs, &d_struct.fields)? {
					DisplayMode::FmtStr(str_lit) => {
						let fmt_args = fmt_str_args(&d_struct.fields);
						quote! {
							let #item_ident #destruct_body = self;
							::std::write!(_col_display_formatter, #str_lit, #fmt_args)
						}
					}
					DisplayMode::FmtExpr(fmt_expr) => {
						quote! {
							let #item_ident #destruct_body = self;
							::std::write!(_col_display_formatter, #fmt_expr)
						}
					}
					DisplayMode::Forward(field_index) => {
						let field = d_struct.fields.iter().nth(field_index).unwrap();
						let ident = field
							.ident
							.clone()
							.unwrap_or_else(|| quote::format_ident!("v{}", field_index));
						quote! {
							let #item_ident #destruct_body = self;
							::std::fmt::Display::fmt(#ident, _col_display_formatter)
						}
					}
				};

			Ok(quote! {
				impl #impl_generics ::std::fmt::Display for #item_ident #ty_generics #where_clause {
					fn fmt(&self, _col_display_formatter: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
						#body
					}
				}
			})
		}
		syn::Data::Enum(ref d_enum) => {
			let match_body = d_enum
				.variants
				.iter()
				.map(|variant| -> Result<TokenStream, syn::Error> {
					let variant_name = &variant.ident;
					let destruct_body = utils::fields_pattern(&variant.fields, |i, field| {
						(
							true,
							field
								.ident
								.clone()
								.unwrap_or_else(|| quote::format_ident!("v{}", i)),
						)
					});

					match DisplayMode::mode_for_variant(&variant.ident, &variant.attrs, &variant.fields)? {
						DisplayMode::FmtStr(str_lit) => {
							let fmt_args = fmt_str_args(&variant.fields);
							Ok(quote! {
								#item_ident :: #variant_name #destruct_body => ::std::write!(_col_display_formatter, #str_lit, #fmt_args),
							})
						}
						DisplayMode::FmtExpr(fmt_expr) => Ok(quote! {
							#item_ident :: #variant_name #destruct_body => ::std::write!(_col_display_formatter, #fmt_expr),
						}),
						DisplayMode::Forward(field_index) => {
							let field = variant.fields.iter().nth(field_index).unwrap();
							let ident = field
								.ident
								.clone()
								.unwrap_or_else(|| quote::format_ident!("v{}", field_index));
							Ok(quote! {
								#item_ident :: #variant_name #destruct_body => ::std::fmt::Display::fmt(#ident, _col_display_formatter),
							})
						}
					}
				})
				.collect::<Result<TokenStream, _>>()?;

			Ok(quote! {
				impl #impl_generics ::std::fmt::Display for #item_ident #ty_generics #where_clause {
					fn fmt(&self, _col_display_formatter: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
						match self {
							#match_body
						}
					}
				}
			})
		}
		syn::Data::Union(_) => Err(syn::Error::new(
			Span::call_site(),
			"Deriving display for union not implemented",
		)),
	}
}

fn fmt_str_args(fields: &syn::Fields) -> TokenStream {
	fields
		.iter()
		.enumerate()
		.map(|(i, field)| {
			field
				.ident
				.as_ref()
				.map(|ident| quote! {#ident=#ident,})
				.unwrap_or_else(|| {
					let ident = quote::format_ident!("v{}", i);
					quote! {#ident,}
				})
		})
		.collect::<TokenStream>()
}

pub enum DisplayMode {
	FmtStr(syn::LitStr),
	FmtExpr(TokenStream),
	Forward(usize),
}
impl DisplayMode {
	pub fn mode_for_variant(
		ident: &syn::Ident,
		attrs: &[syn::Attribute],
		fields: &syn::Fields,
	) -> Result<Self, syn::Error> {
		if let Some(attr) = utils::unique_attr(attrs, |attr| attr.path.is_ident(COL_DISPLAY_ATTR))?
		{
			Self::parse_variant.parse2(attr.tokens.clone())
		} else if let Some((field, attr)) =
			utils::find_unique_attr_in_fields(fields, |attr| attr.path.is_ident(COL_DISPLAY_ATTR))?
		{
			Self::check_field_attr(attr)?;
			Ok(DisplayMode::Forward(
				fields
					.iter()
					.position(|f| f as *const _ == field as *const _)
					.unwrap(),
			))
		} else if fields.iter().count() == 1 {
			Ok(DisplayMode::Forward(0))
		} else {
			Err(syn::Error::new(ident.span(), "Data structures deriving Display and containing multiple fields must have a display attribute"))
		}
	}

	// Parses options in the variant position or struct item position
	pub fn parse_variant(input: syn::parse::ParseStream) -> Result<Self, syn::Error> {
		let lh = input.lookahead1();
		if lh.peek(syn::Token!(=)) {
			input.parse::<syn::Token!(=)>()?;
			Ok(DisplayMode::FmtStr(input.parse()?))
		} else if lh.peek(syn::token::Paren) {
			let content;
			let _paren = syn::parenthesized!(content in input);
			Ok(DisplayMode::FmtExpr(content.parse()?))
		} else {
			Err(lh.error())
		}
	}

	fn check_field_attr(attr: &syn::Attribute) -> Result<(), syn::Error> {
		if attr.tokens.is_empty() {
			Ok(())
		} else {
			Err(syn::Error::new_spanned(
				&attr.tokens,
				"Display attribute has no arguments",
			))
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::utils::check_parses;

	#[test]
	fn enum_struct_variant() {
		let derive_body = col_display_impl(quote! {
			#[derive(Display)]
			enum Foo {
				#[display="{baz} {alice}"]
				Bar {
					baz: u32,
					alice: u32,
				}
			}
		})
		.unwrap();
		check_parses::<syn::File>(derive_body);
	}
}
