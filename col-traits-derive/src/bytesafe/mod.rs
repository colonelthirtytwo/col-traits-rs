use proc_macro2::{
	Span,
	TokenStream,
};
use quote::quote;

pub fn col_bytesafe_impl(ast: TokenStream) -> Result<TokenStream, syn::Error> {
	let ast = syn::parse2::<syn::DeriveInput>(ast)?;
	let ident = &ast.ident;
	let fields = match ast.data {
		syn::Data::Struct(ref d_struct) => d_struct.fields.iter(),
		syn::Data::Enum(_) => {
			return Err(syn::Error::new(
				Span::call_site(),
				"Can't derive ByteSafe on an enum",
			));
		}
		syn::Data::Union(ref d_union) => d_union.fields.named.iter(),
	};

	let check_body = fields
		.map(|field| {
			let ty = &field.ty;
			quote! { <#ty as ::col_traits::ByteSafe>::_byte_safe_derive_check(); }
		})
		.collect::<TokenStream>();

	let mut my_generics = ast.generics.clone();
	for param in my_generics.params.iter_mut() {
		match param {
			syn::GenericParam::Type(ref mut ty_param) => {
				ty_param
					.colon_token
					.get_or_insert_with(|| Default::default());
				ty_param
					.bounds
					.push(syn::parse_quote! { ::col_traits::ByteSafe });
			}
			_ => {}
		}
	}

	let (impl_generics, ty_generics, where_clause) = my_generics.split_for_impl();

	Ok(quote! {
		unsafe impl #impl_generics ::col_traits::ByteSafe for #ident #ty_generics #where_clause {
			fn _byte_safe_derive_check() {
				#check_body
			}
		}
	})
}
