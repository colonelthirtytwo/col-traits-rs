mod field_info;
mod variant_info;

mod kw {
	syn::custom_keyword!(flatten);
	syn::custom_keyword!(from);
	syn::custom_keyword!(source);
}

use proc_macro2::TokenStream;
use quote::quote;

use variant_info::VariantInfo;

pub fn col_error_impl(ast: TokenStream) -> Result<TokenStream, syn::Error> {
	let ast = syn::parse2::<syn::DeriveInput>(ast)?;
	let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();

	let enum_info = match ast.data {
		syn::Data::Enum(ref v) => v,
		_ => {
			return Err(syn::Error::new_spanned(
				ast,
				"#[derive(ColError)] only supports enums",
			));
		}
	};

	let ident = &ast.ident;

	let variant_infos = enum_info
		.variants
		.iter()
		.cloned()
		.map(VariantInfo::parse_variant)
		.collect::<Result<Vec<_>, _>>()?;

	let from_impls = variant_infos
		.iter()
		.map(|vinfo| vinfo.from_impl(ident, &ast.generics))
		.collect::<TokenStream>();
	let source_body = variant_infos
		.iter()
		.map(|vinfo| vinfo.source_match(ident))
		.collect::<TokenStream>();

	Ok(quote! {
		impl #impl_generics ::std::error::Error for #ident #ty_generics #where_clause {
			fn source(&self) -> Option<&(dyn ::std::error::Error + 'static)> {
				match self {
					#source_body
				}
			}
			fn cause(&self) -> Option<&dyn ::std::error::Error> {
				self.source()
			}
		}

		#from_impls
	})
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::utils::check_parses;

	#[test]
	fn empty_enum() {
		let derive_body = col_error_impl(quote! {
			#[derive(Error)]
			enum Foo {
			}
		})
		.unwrap();
		check_parses::<syn::File>(derive_body);
	}

	#[test]
	fn io_err_wrapper() {
		let derive_body = col_error_impl(quote! {
			#[derive(ColError,Debug,Display)]
			enum Foo {
				#[col_error(from,source)]
				IO(std::io::Error),
				Misc,
			}
		})
		.unwrap();
		check_parses::<syn::File>(derive_body);
	}

	#[test]
	fn flatten() {
		let derive_body = col_error_impl(quote! {
			#[derive(Debug, Display, Error)]
			pub enum SpecificError {
				#[error(flatten)]
				Generic(GenericError),
				#[display="Request area too large"]
				TooBig,
				#[display="Request area too small"]
				TooSmall,
				#[error(from, source)]
				FormatError(std::fmt::Error),
			}
		})
		.unwrap();
		check_parses::<syn::File>(derive_body);
	}

	#[test]
	fn flatten_generic() {
		let derive_body = col_error_impl(quote! {
			#[derive(Debug, Display, Error)]
			pub enum ErrorWithGeneric<T: Debug> {
				#[error(flatten)]
				Common(CommonError),

				#[display("Specific error")]
				Specific(T),
			}
		})
		.unwrap();
		check_parses::<syn::File>(derive_body);
	}
}
