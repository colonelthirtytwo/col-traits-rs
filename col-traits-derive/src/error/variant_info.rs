use proc_macro2::TokenStream;
use quote::{
	quote,
	ToTokens,
};
use syn::spanned::Spanned;

use super::{
	field_info::FieldInfo,
	kw,
};
use crate::{
	utils,
	COL_ERR_ATTR,
};

pub struct VariantInfo {
	pub variant: syn::Variant,
	pub flatten: bool,
	pub from: bool,

	pub source_field: Option<usize>,
}
impl VariantInfo {
	pub fn parse_variant(variant: syn::Variant) -> Result<Self, syn::Error> {
		let mut info = VariantInfo {
			variant,
			flatten: false,
			from: false,
			source_field: None,
		};

		let metas = info
			.variant
			.attrs
			.iter()
			.filter(|attr| attr.path.is_ident(COL_ERR_ATTR))
			.cloned()
			.collect::<Vec<_>>();

		for meta in metas.into_iter() {
			for attr in meta.parse_args_with(VariantAttribute::parse_attr)? {
				info.apply_attr(attr)?;
			}
		}

		let fields = info
			.variant
			.fields
			.iter()
			.cloned()
			.enumerate()
			.map(|(i, field)| FieldInfo::parse_field(i, field))
			.collect::<Result<Vec<_>, _>>()?;

		let source_field = utils::check_unique(fields.iter().filter(|f| f.source.is_some()))
			.map_err(|dup| {
				syn::Error::new(
					dup.source.as_ref().unwrap().span(),
					"Only one field may have a source attribute",
				)
			})?;
		if let Some(source_field) = source_field {
			if info.source_field.is_some() {
				return Err(syn::Error::new(
					source_field.source.as_ref().unwrap().span(),
					"Can't have source attribute on both field and variant",
				));
			}
			info.source_field = Some(source_field.field_index);
		}

		Ok(info)
	}

	fn apply_attr(&mut self, attr: VariantAttribute) -> Result<(), syn::Error> {
		match attr {
			VariantAttribute::Flatten(kw) => {
				if self.flatten {
					return Err(syn::Error::new(kw.span(), "duplicate flatten attribute"));
				}
				if self.from {
					return Err(syn::Error::new(
						kw.span(),
						"flatten and from are mutually exclusive",
					));
				}
				if self.source_field.is_some() {
					return Err(syn::Error::new(
						kw.span(),
						"flatten and source are mutually exclusive",
					));
				}
				if self.variant.fields.iter().count() != 1 {
					return Err(syn::Error::new(kw.span(), "flatten attribute requires exactly one field that implements std::error::Error"));
				}
				self.flatten = true;
			}
			VariantAttribute::From(kw) => {
				if self.from {
					return Err(syn::Error::new(kw.span(), "duplicate from attribute"));
				}
				if self.flatten {
					return Err(syn::Error::new(
						kw.span(),
						"flatten and from are mutually exclusive",
					));
				}
				if self.variant.fields.iter().count() != 1 {
					return Err(syn::Error::new(
						kw.span(),
						"from attribute requires exactly one field",
					));
				}
				self.from = true;
			}
			VariantAttribute::Source(kw) => {
				if self.source_field.is_some() {
					return Err(syn::Error::new(kw.span(), "duplicate source attribute"));
				}
				if self.flatten {
					return Err(syn::Error::new(
						kw.span(),
						"flatten and source are mutually exclusive",
					));
				}
				if self.variant.fields.iter().count() != 1 {
					return Err(syn::Error::new(
						kw.span(),
						"source attribute requires exactly one field",
					));
				}
				self.source_field = Some(0);
			}
		}
		Ok(())
	}

	pub fn from_impl(&self, typ_name: &syn::Ident, generics: &syn::Generics) -> TokenStream {
		if !self.from && !self.flatten {
			return quote! {};
		}

		let variant_name = &self.variant.ident;
		let field = self.variant.fields.iter().nth(0).unwrap();
		let field_ty = &field.ty;

		if self.flatten {
			let impl_generics = if generics.params.is_empty() {
				quote! {<COL_ERR_T>}
			} else {
				let body = generics.params.to_token_stream();
				quote! {<COL_ERR_T, #body>}
			};
			let ty_generics = utils::ty_generics(generics);
			let mut where_clause =
				generics
					.where_clause
					.clone()
					.unwrap_or_else(|| syn::WhereClause {
						where_token: Default::default(),
						predicates: Default::default(),
					});
			where_clause
				.predicates
				.push(syn::parse_quote! { #field_ty: ::std::convert::From<COL_ERR_T> });

			let initializer = if let Some(ref field_ident) = field.ident {
				quote! { { #field_ident = <#field_ty as ::std::convert::From<COL_ERR_T>>::from(v) } }
			} else {
				quote! { (<#field_ty as ::std::convert::From<COL_ERR_T>>::from(v)) }
			};

			quote! {
				impl #impl_generics From<COL_ERR_T> for #typ_name #ty_generics #where_clause
				{
					fn from(v: COL_ERR_T) -> Self {
						#typ_name::#variant_name #initializer
					}
				}
			}
		} else if self.from {
			let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
			let initializer = if let Some(ref field_ident) = field.ident {
				quote! { { #field_ident = v } }
			} else {
				quote! { (v) }
			};

			quote! {
				impl #impl_generics From<#field_ty> for #typ_name #ty_generics #where_clause {
					fn from(v: #field_ty) -> Self {
						#typ_name::#variant_name #initializer
					}
				}
			}
		} else {
			unreachable!();
		}
	}

	pub fn source_match(&self, typ_name: &syn::Ident) -> TokenStream {
		let variant_name = &self.variant.ident;

		let field_index = if self.flatten {
			0
		} else if let Some(i) = self.source_field {
			i
		} else {
			let empty_body = utils::fields_match_any(&self.variant.fields);
			return quote! { #typ_name :: #variant_name #empty_body => None, };
		};

		let match_values = utils::fields_pattern(&self.variant.fields, |i, _field| {
			if i == field_index {
				(true, quote! {src})
			} else {
				(false, quote! {_})
			}
		});
		quote! {
			#typ_name :: #variant_name #match_values => Some(src),
		}
	}
}

/// Single meta-list item for the individual enum variants
enum VariantAttribute {
	/// Impl from, cause for all T where field: from<T>
	Flatten(kw::flatten),
	/// Impl from for single-field enum variant
	From(kw::from),
	/// Impl source for single-field enum variant
	Source(kw::source),
}
impl VariantAttribute {
	/// Parses a token stream from a `syn::Attribute`.
	pub fn parse_attr(
		stream: syn::parse::ParseStream,
	) -> Result<syn::punctuated::Punctuated<Self, syn::Token!(,)>, syn::Error> {
		syn::punctuated::Punctuated::parse_terminated_with(stream, Self::parse_one)
	}

	/// Parses one meta list item
	pub fn parse_one(input: syn::parse::ParseStream) -> Result<Self, syn::Error> {
		let lh = input.lookahead1();
		if lh.peek(kw::flatten) {
			Ok(VariantAttribute::Flatten(input.parse()?))
		} else if lh.peek(kw::from) {
			Ok(VariantAttribute::From(input.parse()?))
		} else if lh.peek(kw::source) {
			Ok(VariantAttribute::Source(input.parse()?))
		} else {
			Err(lh.error())
		}
	}
}
