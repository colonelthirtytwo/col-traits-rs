use syn::spanned::Spanned;

use super::kw;
use crate::COL_ERR_ATTR;

pub struct FieldInfo {
	pub field: syn::Field,
	pub field_index: usize,
	pub source: Option<kw::source>,
}
impl FieldInfo {
	pub fn parse_field(field_index: usize, field: syn::Field) -> Result<Self, syn::Error> {
		let mut info = FieldInfo {
			field,
			field_index,
			source: None,
		};

		let metas = info
			.field
			.attrs
			.iter()
			.filter(|attr| attr.path.is_ident(COL_ERR_ATTR))
			.cloned()
			.collect::<Vec<_>>();

		for meta in metas.into_iter() {
			for attr in meta.parse_args_with(FieldAttribute::parse_attr)? {
				info.apply_attr(attr)?;
			}
		}
		Ok(info)
	}

	fn apply_attr(&mut self, attr: FieldAttribute) -> Result<(), syn::Error> {
		match attr {
			FieldAttribute::Source(kw) => {
				if self.source.is_some() {
					return Err(syn::Error::new(kw.span(), "duplicate source attribute"));
				}
				self.source = Some(kw.clone());
			}
		};
		Ok(())
	}
}

pub enum FieldAttribute {
	Source(kw::source),
}

impl FieldAttribute {
	/// Parses a token stream from a `syn::Attribute`.
	pub fn parse_attr(
		stream: syn::parse::ParseStream,
	) -> Result<syn::punctuated::Punctuated<Self, syn::Token!(,)>, syn::Error> {
		syn::punctuated::Punctuated::parse_terminated_with(stream, Self::parse_one)
	}

	/// Parses one meta list item
	pub fn parse_one(input: syn::parse::ParseStream) -> Result<Self, syn::Error> {
		let lh = input.lookahead1();
		if lh.peek(kw::source) {
			Ok(FieldAttribute::Source(input.parse()?))
		} else {
			Err(lh.error())
		}
	}
}
