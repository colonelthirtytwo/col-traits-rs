use proc_macro2::{
	Span,
	TokenStream,
};
use quote::{
	quote,
	ToTokens,
};

use crate::{
	utils,
	COL_DEREF_ATTR,
};

pub fn col_deref_impl(ast: TokenStream, with_mut: bool) -> Result<TokenStream, syn::Error> {
	let ast = syn::parse2::<syn::DeriveInput>(ast)?;
	let ident = &ast.ident;
	let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();
	match ast.data {
		syn::Data::Struct(ref d_struct) => {
			let field = deref_field(&d_struct.fields)?;
			let field_ident = field
				.ident
				.as_ref()
				.map(|id| id.to_token_stream())
				.unwrap_or_else(|| {
					let i = d_struct
						.fields
						.iter()
						.position(|f| field as *const _ == f as *const _)
						.unwrap();
					syn::Member::Unnamed(i.into()).into_token_stream()
				});
			let field_ty = &field.ty;

			let deref_impl = quote! {
				impl #impl_generics ::std::ops::Deref for #ident #ty_generics #where_clause {
					type Target = #field_ty;
					fn deref(&self) -> &Self::Target {
						&self.#field_ident
					}
				}
			};

			let deref_mut_impl = if with_mut {
				Some(quote! {
					impl #impl_generics ::std::ops::DerefMut for #ident #ty_generics #where_clause {
						fn deref_mut(&mut self) -> &mut Self::Target {
							&mut self.#field_ident
						}
					}
				})
			} else {
				None
			};

			Ok(quote! { #deref_impl #deref_mut_impl })
		}
		syn::Data::Enum(ref d_enum) => {
			if d_enum.variants.is_empty() {
				// Can't know Target type
				return Err(syn::Error::new(
					Span::call_site(),
					"Cannot derive Deref(Mut) for an enum with no variants",
				));
			}

			let deref_fields = d_enum
				.variants
				.iter()
				.map(|variant| deref_field(&variant.fields))
				.collect::<Result<Vec<_>, _>>()?;
			let deref_body = d_enum
				.variants
				.iter()
				.zip(deref_fields.iter().copied())
				.map(|(variant, deref_field)| {
					let variant_ident = &variant.ident;
					let destructor = utils::fields_pattern(&variant.fields, |_, f| {
						if f as *const _ == deref_field as *const _ {
							(true, quote! {the_value})
						} else {
							(false, quote! {_})
						}
					});
					quote! { #ident :: #variant_ident #destructor => the_value, }
				})
				.collect::<TokenStream>();
			let deref_mut_body = d_enum
				.variants
				.iter()
				.zip(deref_fields.iter().copied())
				.map(|(variant, deref_field)| {
					let variant_ident = &variant.ident;
					let destructor = utils::fields_pattern(&variant.fields, |_, f| {
						if f as *const _ == deref_field as *const _ {
							(true, quote! {mut the_value})
						} else {
							(false, quote! {_})
						}
					});
					quote! { #ident :: #variant_ident #destructor => the_value, }
				})
				.collect::<TokenStream>();

			let field_ty = &deref_fields[0].ty;

			let deref_impl = quote! {
				impl #impl_generics ::std::ops::Deref for #ident #ty_generics #where_clause {
					type Target = #field_ty;
					fn deref(&self) -> &Self::Target {
						match self {
							#deref_body
						}
					}
				}
			};

			let deref_mut_impl = if with_mut {
				Some(quote! {
					impl #impl_generics ::std::ops::DerefMut for #ident #ty_generics #where_clause {
						fn deref_mut(&mut self) -> &mut Self::Target {
							match self {
								#deref_mut_body
							}
						}
					}
				})
			} else {
				None
			};

			Ok(quote! { #deref_impl #deref_mut_impl })
		}
		syn::Data::Union(_) => {
			return Err(syn::Error::new(
				Span::call_site(),
				"Cannot derive Deref(Mut) on a union",
			));
		}
	}
}

fn deref_field(fields: &syn::Fields) -> Result<&syn::Field, syn::Error> {
	utils::find_unique_attr_in_fields(fields, |attr| attr.path.is_ident(COL_DEREF_ATTR))?
		.map(|(field, attr)| {
			if attr.tokens.is_empty() {
				Ok(field)
			} else {
				Err(syn::Error::new_spanned(
					&attr.tokens,
					format!("#[{}] attribute does not take arguments", COL_DEREF_ATTR),
				))
			}
		})
		.transpose()?
		.or_else(|| {
			if fields.iter().count() == 1 {
				fields.iter().next()
			} else {
				None
			}
		})
		.ok_or_else(|| {
			syn::Error::new_spanned(
				fields,
				format!(
					"Deref derive requires #[{}] attribute on field to deref",
					COL_DEREF_ATTR
				),
			)
		})
}
