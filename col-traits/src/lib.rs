//! A bunch of derive procedural macros for common scenarios
//!
//! `#[derive(Display)]`
//! ====================
//!
//! Implements `std::fmt::Display` without boilerplate.
//!
//! Can be used on structs or enums. For enums, the display string is per-variant.
//!
//! The struct or variant is formatted based on the following rules, which are mutually exclusive:
//!
//! * If the struct or variant has only one field, it will call `fmt` on that field.
//! * If any field in the struct or variant has the `#[display]` attribute, it will call `fmt` on that
//!   field, ignoring the other fields.
//! * If the struct or variant itself has an attribute of the form `#[display="fmtstr"]`, it will format
//!   using `write!(formatter, "fmtstr", <all fields>)`. Use `{field_name}` to interpolate named fields,
//!   and `{0}`, `{1}`, ... to interpolate unnamed fields. All fields must be used in the format string.
//! * If the struct or variant itself has an attribute of the form `#[display("fmtstr", expr1, expr2, ...)]`,
//!   it will format using `write!(formatter, "fmtstr", expr1, expr2, ...)`. Named fields are available using
//!   their identifiers. Unnamed fields are available as the variables `v0`, `v1`, `v2`, etc..
//! * Otherwise, the derive will fail to compile.
//!
//! `#[derive(Error)]`
//! ==================
//!
//! Implements `std::error::Error` on enums. Since the trait requires `Debug` and `Display`, you will probably
//! want to derive those as well.
//!
//! An example:
//!
//! ```rust
//! use col_traits::{Error, Display};
//!
//! #[derive(Debug,Display,Error)]
//! pub enum Error {
//! 	/// Wrap an IO error, implement `From<io::Error>`, and return it from `source`/`cause`.
//! 	#[error(from,source)]
//! 	IO(std::io::Error),
//!
//! 	/// Unit error with display string
//! 	#[display="Process Interrupted"]
//! 	Interrupted,
//!
//! 	/// Error with fields
//! 	#[display="File not found on server {server}, path {path}"]
//! 	NotFound {
//! 		server: String,
//! 		path: String,
//! 	}
//! }
//! ```
//!
//! Variant attributes:
//! -------------------
//!
//! * `#[error(from)]`: Implements `From<T>` where `T` is the type of the field. Only usable on variants with
//!   one field.
//! * `#[error(source)]`: `Error.soruce` and `Error.cause` will return the inner field. Only usable on
//!   variants with one field; for multi-field variants, use `#[error(source)]` on the field to specify
//!   which field to return.
//! * `#[error(flatten)]`: Flattens another error into this one, so that types compatible with the inner
//!   error can be returned as well.
//!   
//!   Specifically, implements `From<T> where Inner: From<T>` where `Inner` is the type of the field,
//!   and returns the field from `Error.source` and `Error.cause`, as with `source`.
//!   
//!   Incompatible with `from` and `source`. Only usable on variants with one field.
//!
//! Field attributes:
//! -----------------
//!
//! * `#[error(source)]`: Returns the marked field from `Error.source` and `Error.cause`. Must only be
//!   on one field.
//!
//! `#[derive(Deref)]`/`#[derive(DerefMut)]`
//! ========================================
//!
//! Implements `std::ops::Deref`, and optionally `std::ops::DerefMut`, on a struct or enum, returning a
//! reference to one of the fields.
//!
//! Field attributes:
//! -----------------
//!
//! * `#[deref]`: Picks the field to deref into. For enums, needs one per variant. Can be omitted
//!   for single-field structs or variants.
//!
//! `ByteSafe`/`#[derive(ByteSafe)]`
//! ================================
//!
//! Trait for types that can be safely converted to/from byte slices.
//!
//! This trait allows only types where any byte value is valid, including integer and float types, but
//! excluding references or enums. Useful for reading or writing types from a generic byte buffer.
//!
//! Use `#[derive(ByteSafe)]` to safely implement `ByteSafe` on a struct or union. This will ensure the
//! fields all implement `ByteSafe`. It has no options or attributes.
//!
//! Example:
//!
//! ```rust
//! use col_traits::ByteSafe;
//! #[derive(Debug, Clone, Copy, ByteSafe)]
//! pub struct Vertex {
//! 	position: [f32; 4],
//! 	normal: [f32; 3],
//! 	color: [f32; 3],
//! }
//! ```
//!

#![allow(unused_imports)]

pub mod bytesafe;
pub use self::bytesafe::ByteSafe;

#[macro_use]
extern crate col_traits_derive;
pub use col_traits_derive::{
	ByteSafe,
	Deref,
	DerefMut,
	Display,
	Error,
};

// Proc macro related:

#[cfg(feature = "proc-macro-traits")]
pub mod derive_params;
#[cfg(feature = "proc-macro-traits")]
pub use self::derive_params::{
	FieldParameters,
	ItemParameters,
	VariantParameters,
};

#[cfg(feature = "proc-macro-traits")]
#[macro_use]
extern crate col_traits_derive_params;
#[cfg(feature = "proc-macro-traits")]
pub use col_traits_derive_params::DeriveParams;

#[doc(hidden)]
/// Re-exports for access inside macros
pub mod _reexports {
	#[cfg(feature = "proc-macro-traits")]
	pub use ::proc_macro2;
	#[cfg(feature = "proc-macro-traits")]
	pub use ::syn;
}
