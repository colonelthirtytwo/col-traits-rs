pub trait ItemParameters: Sized {
	fn parse_params(item: &syn::DeriveInput) -> Result<Self, syn::Error>;
}

pub trait VariantParameters: Sized {
	fn parse_params(item: &syn::Variant) -> Result<Self, syn::Error>;
}

pub trait FieldParameters: Sized {
	fn parse_params(item: &syn::Field) -> Result<Self, syn::Error>;
}
