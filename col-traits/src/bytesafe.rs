//! Module for working with byte-safe types. See the `ByteSafe` trait for more information.

/// Trait for types that can be safely converted to and from a byte slice.
///
/// This is a subset of `Copy`, but with the additional restriction that all byte
/// patterns are allowed. This disallows references, enums, etc.
///
/// The best way to implement this trait is with `#[derive(ByteSafe)]`; it will
/// add checks to make sure the fields of your type all implement `ByteSafe`. If
/// you're very sure, you can also `unsafe impl` the trait manually.
///
/// Some noteworthy types that are _not_ `ByteSafe`:
///
/// * Any `enum`: enums with invalid tags cause undefined behavior. The one exception is
///   `Option<NonNull<T>>` since it has the same representation as `*mut T`.
/// * `bool`: There's only two compiler-dependent bit representations that work: `true` or `false`.
///   Anything else is UB.
/// * Tuples: Rust book says their layout is not defined and may change, so currently not yet
///   implemented. May change. Exception: the empty tuple is well defined and implements `ByteSafe`.
///
/// Some things to keep in mind when using this:
///
/// * Unless you are using `#[repr(C)]`, Rust can lay out the structure however it wishes,
///   changing between compiler versions or other factors. If you are transferring data
///   between processes, make sure your types are `#[repr(C)]`.
/// * This does not do any endian conversion.
pub unsafe trait ByteSafe: Copy + Sized + 'static {
	#[doc(hidden)]
	/// Dummy method that the derive generates to check that the fields of a
	/// structure, enum, or union implement ByteSafe.
	///
	/// If you get a compile error in this function your structure probably
	/// includes at least one non-ByteSafe field.
	fn _byte_safe_derive_check() {}
}

/// Converts a byte slice to a reference to a byte-safe value.
///
/// If the slice is at least the type's size and is aligned correctly for the type,
/// returns the start of the byte slice casted to the type (which is safe).
///
/// If the slice is too small, or the address of the start of the slice is not
/// a multiple of the type's alignment, returns an appropriate error.
///
/// Panics
/// ------
///
/// If the type has a size of zero.
pub fn as_value<'a, T: ByteSafe>(bytes: &'a [u8]) -> Result<&'a T, AsValueError> {
	let val_slice = as_values_slice(bytes)?;
	if val_slice.len() == 0 {
		return Err(AsValueError::TooSmall);
	}
	Ok(&val_slice[0])
}

/// Converts a reference to a byte-safe value to a byte slice covering the whole value.
pub fn as_bytes<'a, T: ByteSafe>(val: &'a T) -> &'a [u8] {
	slice_as_bytes(std::slice::from_ref(val))
}

/// Converts a byte slice into a slice of byte-safe values.
///
/// It will convert as much of the byte slice as possible, rounding down if the byte
/// slice's length is not a multiple of the type's size.
///
/// This will never return `Err(AsValueError::TooSmall)`; it will return a zero-length
/// slice instead. It can, however, return `Err(AsValueError::Misaligned)`.
///
/// Panics
/// ------
///
/// If the type has a size of zero.
pub fn as_values_slice<'a, T: ByteSafe>(bytes: &'a [u8]) -> Result<&'a [T], AsValueError> {
	if bytes.as_ptr().align_offset(std::mem::align_of::<T>()) != 0 {
		return Err(AsValueError::Misaligned);
	}
	let num_elements = bytes.len() / std::mem::size_of::<T>();
	Ok(unsafe { std::slice::from_raw_parts(bytes.as_ptr() as *const T, num_elements) })
}

/// Converts a slice of byte-safe values into a bytes slice covering the entire passed in slice.
pub fn slice_as_bytes<'a, T: ByteSafe>(vals: &'a [T]) -> &'a [u8] {
	let num_bytes = vals.len() * std::mem::size_of::<T>();
	unsafe { std::slice::from_raw_parts(vals.as_ptr() as *const u8, num_bytes) }
}

/// Errors that may occur when casting a byte slice to a byte-safe value
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum AsValueError {
	/// The provided byte slice's length was less than the size of the type
	TooSmall,
	/// The address of the start of the byte slice does not match the alignment needed
	/// for the type
	Misaligned,
}
impl std::fmt::Display for AsValueError {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			AsValueError::TooSmall => f.write_str("Byte slice was too small for the type"),
			AsValueError::Misaligned => {
				f.write_str("Byte slice's alignment does not match alignment for the type")
			}
		}
	}
}
impl std::error::Error for AsValueError {}

// Basic Bytesafe Impls

// Primitives
unsafe impl ByteSafe for u8 {}
unsafe impl ByteSafe for u16 {}
unsafe impl ByteSafe for u32 {}
unsafe impl ByteSafe for u64 {}
unsafe impl ByteSafe for u128 {}
unsafe impl ByteSafe for i8 {}
unsafe impl ByteSafe for i16 {}
unsafe impl ByteSafe for i32 {}
unsafe impl ByteSafe for i64 {}
unsafe impl ByteSafe for i128 {}
unsafe impl ByteSafe for f32 {}
unsafe impl ByteSafe for f64 {}
unsafe impl ByteSafe for char {}
unsafe impl<T: 'static> ByteSafe for *const T {}
unsafe impl<T: 'static> ByteSafe for *mut T {}

// Empty types
unsafe impl ByteSafe for () {}
unsafe impl<T: ByteSafe> ByteSafe for std::marker::PhantomData<T> {}

// Transparent wrappers
unsafe impl<T: ByteSafe> ByteSafe for std::mem::MaybeUninit<T> {}
unsafe impl<T: ByteSafe> ByteSafe for std::mem::ManuallyDrop<T> {}
unsafe impl<T: 'static> ByteSafe for Option<std::ptr::NonNull<T>> {}

// Arrays
unsafe impl<T: ByteSafe> ByteSafe for [T; 0] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 1] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 2] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 3] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 4] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 5] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 6] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 7] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 8] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 9] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 10] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 11] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 12] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 13] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 14] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 15] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 16] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 17] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 18] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 19] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 20] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 21] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 22] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 23] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 24] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 25] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 26] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 27] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 28] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 29] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 30] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 31] {}
unsafe impl<T: ByteSafe> ByteSafe for [T; 32] {}
