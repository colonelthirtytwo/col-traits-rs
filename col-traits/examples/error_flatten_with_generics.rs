use col_traits::{
	Display,
	Error,
};
use std::fmt::Debug;

#[derive(Debug, Display, Error)]
pub enum CommonError {
	#[error(from, source)]
	IO(std::io::Error),

	#[error(from)]
	Unrecognized(String),

	#[display = "Interrupted"]
	Interrupted,
}

/// Works with generics too!
#[derive(Debug, Display, Error)]
pub enum ErrorWithGeneric<T: Debug> {
	#[error(flatten)]
	Common(CommonError),

	#[display("Specific error")]
	Specific(T),
}

fn main() {
	let _err1 = ErrorWithGeneric::<u32>::Specific(123);
	let _err2: ErrorWithGeneric<String> = CommonError::Interrupted.into();
}
