use col_traits::{
	bytesafe,
	ByteSafe,
};

#[derive(Debug, Clone, Copy, ByteSafe)]
pub struct Vertex {
	position: [f32; 4],
	normal: [f32; 3],
	color: [f32; 3],
}

// // This doesn't compile; `msg` may not be valid after reading as bytes
// #[derive(Debug, Clone, Copy, ByteSafe)]
// pub struct HasAReference {
// 	msg: &'static str,
// }

fn append_vertex(buf: &mut Vec<u8>, vtx: Vertex) {
	buf.extend_from_slice(bytesafe::as_bytes(&vtx));
}

fn main() {
	let mut vertex_buffer = Vec::new();

	append_vertex(
		&mut vertex_buffer,
		Vertex {
			position: [0., 0., 0., 1.],
			normal: [0., 0., 1.],
			color: [1., 0., 0.],
		},
	);

	append_vertex(
		&mut vertex_buffer,
		Vertex {
			position: [1., 0., 0., 1.],
			normal: [0., 0., 1.],
			color: [0., 1., 0.],
		},
	);

	append_vertex(
		&mut vertex_buffer,
		Vertex {
			position: [0., 1., 0., 1.],
			normal: [0., 0., 1.],
			color: [0., 0., 1.],
		},
	);

	println!("Raw bytes: {:?}", vertex_buffer);
	println!(
		"As vertices: {:#?}",
		bytesafe::as_values_slice::<Vertex>(&vertex_buffer)
	);
}
