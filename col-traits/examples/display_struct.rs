use col_traits::Display;

#[derive(Display)]
#[display = "{schema}://{host}/{path}"]
pub struct Url {
	pub schema: String,
	pub host: String,
	pub path: String,
}

fn main() {}
