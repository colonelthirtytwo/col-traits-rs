use col_traits::{
	Display,
	Error,
};

/// An example error!
#[derive(Debug, Display, Error)]
pub enum MyError {
	/// An IO error happened!
	///
	/// `from` means this will `impl From<std::io::Error> for MyError`,
	/// `source` means `my_error.source()` and `my_error.cause()` will return the inner error.
	/// Because there's only one field, `Display` will just forward to the inner field's `Display`
	/// implementation.
	#[error(from, source)]
	IO(std::io::Error),

	/// Some random error happened!
	///
	/// Mostly same as above, but can't specify `source` since `String` does not implement `std::error::Error`.
	#[error(from)]
	StringErr(String),

	/// Empty variant. Need to set a display string.
	#[display = "Process Interrupted"]
	Interrupted,

	/// Variant with more than one field. Also needs a display string.
	///
	/// When using `display="fmtstr"`, fields get passed as arguments.
	/// You must use all the fields, otherwise rust will throw an error
	#[display = "HTTP code {code}: {response:?}"]
	HttpError { code: u32, response: Vec<u8> },

	/// Same as above, but this time its a tuple struct
	#[display = "HTTP code {0}: {1:?}"]
	HttpErrorTuple(u32, Vec<u8>),

	/// If you can't use all the fields, or need to use an expression, you can use an alternate syntax.
	///
	/// Field names are available.
	#[display("Could not process {}: {}", item.name, message)]
	ProcessingError { message: String, item: Item },

	/// Works for tuple fields as well. Fields are available using the names `v0`, `v1`, `v2`, ...
	#[display("Could not process {}: {}", v1.name, v0)]
	ProcessingErrorTuple(String, Item),

	/// For multi-field variants, you can use the `source` attribute to set up what
	/// field that the `source` or `cause` methods returns.
	#[display = "IO error while processing index {index}: {err}"]
	WrappedErrorWithInfo {
		index: u32,
		#[error(source)]
		err: std::io::Error,
	},
}

#[derive(Debug)]
pub struct Item {
	// Pretend there's something complicated here
	pub name: String,
}

fn main() {
	assert_eq!(
		MyError::from(String::from("hello world")).to_string(),
		"hello world"
	);
	assert_eq!(MyError::Interrupted.to_string(), "Process Interrupted");
	assert_eq!(
		MyError::HttpError {
			code: 404,
			response: vec![]
		}
		.to_string(),
		"HTTP code 404: []"
	);

	println!("Everything worked OK!");
}
