use col_traits::{
	Display,
	Error,
};

/// Generic error type common to many errors
#[derive(Debug, Display, Error)]
pub enum GenericError {
	#[error(from, source)]
	IO(std::io::Error),

	#[error(from)]
	Unrecognized(String),

	#[display = "Interrupted"]
	Interrupted,
}

/// Specififc error type to one function
#[derive(Debug, Display, Error)]
pub enum SpecificError {
	/// Specifying `flatten` is like specifying `from` and `source`, but also
	/// implements `From<T> where InnerError: From<T>`. In this case,
	/// `SpecificError` will implement `From<GenericError>`, `From<std::io::Error>`
	/// and `From<String>`
	#[error(flatten)]
	Generic(GenericError),

	/// ...then afterwards, you can add custom error types as well
	#[display = "Request area too large"]
	TooBig,
	#[display = "Request area too small"]
	TooSmall,

	/// You can add other `from`s as long as they don't conflict
	#[error(from, source)]
	FormatError(std::fmt::Error),
}

fn main() {
	let _err1 = SpecificError::from(String::from("Hello world"));
}
