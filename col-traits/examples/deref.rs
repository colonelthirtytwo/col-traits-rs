use col_traits::{
	Deref,
	DerefMut,
};

#[derive(DerefMut)]
pub struct Wrapper(u32);

#[derive(Deref)]
pub struct WrapperWithMultipleFields {
	#[deref]
	pub data: String,
	pub split_point: usize,
}

#[derive(DerefMut)]
pub enum Options {
	Foo(String),
	Bar {
		#[deref]
		s: String,
		i: usize,
	},
}

fn main() {
	let mut w1 = Wrapper(123);
	assert_eq!(*w1, 123);
	*w1 = 456;
	assert_eq!(*w1, 456);

	let w2 = WrapperWithMultipleFields {
		data: String::from("hello world"),
		split_point: 6,
	};
	assert_eq!(w2.as_str(), "hello world");

	let o1 = Options::Foo(String::from("hello world"));
	assert_eq!(o1.as_str(), "hello world");
	let o2 = Options::Bar {
		s: String::from("goodbye world"),
		i: 42,
	};
	assert_eq!(o2.as_str(), "goodbye world");

	println!("Everything worked OK!");
}
