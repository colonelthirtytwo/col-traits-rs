use col_traits::{
	DeriveParams,
	ItemParameters,
};

#[derive(Debug, DeriveParams)]
#[derive_params(target_struct, super_attr(my_derive))]
struct MyDeriveParams {
	#[derive_params(ident)]
	ident: syn::Ident,

	name: syn::LitStr,
	version: Option<syn::LitStr>,
	use_alternate_format: bool,
}

fn main() {
	let my_struct: syn::DeriveInput = syn::parse_quote! {
		#[derive(MyDerive)]
		#[my_derive(name("Foo"), use_alternate_format)]
		struct Foo;
	};

	let parsed_attrs: MyDeriveParams =
		MyDeriveParams::parse_params(&my_struct).expect("Could not parse attributes");

	println!("Parsed Params: {:#?}", parsed_attrs);
}
