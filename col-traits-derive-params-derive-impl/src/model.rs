use proc_macro2::{
	Span,
	TokenStream,
};
use quote::{
	format_ident,
	quote,
};

use crate::{
	attrs::{
		FieldAttrs,
		ItemAttrs,
	},
	combine_errors,
	type_detection,
};
use syn::spanned::Spanned;

impl ItemAttrs {
	pub fn target_item(&self) -> bool {
		self.target_struct || self.target_enum || self.target_union
	}

	pub fn check(&self) -> Result<(), syn::Error> {
		let mut errors = vec![];
		if self.target_item() && self.target_variant {
			errors.push(syn::Error::new(Span::call_site(), "target_variant cannot be specified with target_struct, target_enum, or target_union"));
		}
		if self.target_item() && self.target_field {
			errors.push(syn::Error::new(
				Span::call_site(),
				"target_field cannot be specified with target_struct, target_enum, or target_union",
			));
		}
		if !self.target_item() && !self.target_variant && !self.target_field {
			errors.push(syn::Error::new(Span::call_site(), "Must specify one of target_struct, target_enum, target_union, target_variant, or target_field"));
		}

		errors.extend(self.fields.iter().filter_map(|f| f.check().err()));

		combine_errors(errors)
	}

	pub fn target(&self) -> ParamTarget {
		if self.target_item() {
			ParamTarget::Item {
				ty_struct: self.target_struct,
				ty_enum: self.target_enum,
				ty_union: self.target_enum,
			}
		} else if self.target_variant {
			ParamTarget::Variant
		} else if self.target_field {
			ParamTarget::Field
		} else {
			panic!("target() called with invalid ItemAttrs");
		}
	}
}

impl FieldAttrs {
	pub fn check(&self) -> Result<(), syn::Error> {
		let mut errors = vec![];
		let num_modes = [
			self.ident,
			self.generics,
			self.ty,
			self.variants,
			self.fields,
			self.skip,
		]
		.iter()
		.copied()
		.filter(|x| *x)
		.count();
		if num_modes > 1 {
			errors.push(syn::Error::new(
				Span::call_site(),
				"Only one field type attribute allowed",
			));
		}

		// TODO: check min_args/max_args
		combine_errors(errors)
	}

	pub fn param_type(&self) -> Result<ParamType, syn::Error> {
		if self.ident {
			Ok(ParamType::Ident)
		} else if self.generics {
			Ok(ParamType::Generics)
		} else if self.ty {
			Ok(ParamType::Type)
		} else if self.variants {
			if let Some(vec_ty) = type_detection::is_vec(&self.field_ty) {
				Ok(ParamType::Variants {
					args_ty: vec_ty.clone(),
				})
			} else {
				Err(syn::Error::new(
					self.field_ty.span(),
					"Variants parsing requires a Vec of types",
				))
			}
		} else if self.fields {
			if let Some(vec_ty) = type_detection::is_vec(&self.field_ty) {
				Ok(ParamType::Fields {
					args_ty: vec_ty.clone(),
				})
			} else {
				Err(syn::Error::new(
					self.field_ty.span(),
					"Fields parsing requires a Vec of types",
				))
			}
		} else if self.skip {
			Ok(ParamType::Skip)
		} else if type_detection::is_prim_typ(&self.field_ty, "bool") {
			Ok(ParamType::Boolean)
		} else if let Some(opt_ty) = type_detection::is_option(&self.field_ty) {
			Ok(ParamType::Optional {
				contents: opt_ty.clone(),
			})
		} else if let Some(vec_ty) = type_detection::is_vec(&self.field_ty) {
			Ok(ParamType::Multi {
				contents: vec_ty.clone(),
				min_args: 0,
				max_args: usize::max_value(),
			})
		} else {
			Ok(ParamType::Required {
				contents: self.field_ty.clone(),
			})
		}
	}

	fn implementation(
		&self,
		this_crate: &TokenStream,
		super_attr_name: &str,
		target: &ParamTarget,
	) -> Result<ParamImpls, syn::Error> {
		let name = self.field_ident.as_ref().unwrap();
		let span = name.span();
		let local_name = format_ident!("arg_{}", name);

		match self.param_type()? {
			ParamType::Ident => match target {
				ParamTarget::Item {..} |
				ParamTarget::Variant => Ok(ParamImpls {
					name: name.clone(),
					local_name: local_name.clone(),
					//ty: Some(quote! { ::std::option::Option<#this_crate ::_reexports::syn::Ident> }),
					ty: Some(quote! { #this_crate ::_reexports::syn::Ident }),
					init: quote! { item.ident.clone() },
					parse_branch: None,
					finish: quote! {#local_name},
				}),
				ParamTarget::Field => Ok(ParamImpls {
					name: name.clone(),
					local_name: local_name.clone(),
					ty: Some(quote! { ::std::option::Option<#this_crate ::_reexports::syn::Ident> }),
					init: quote! { item.ident.clone() },
					parse_branch: None,
					finish: quote! {#local_name},
				}),
			},
			ParamType::Generics => match target {
				ParamTarget::Item {..} => Ok(ParamImpls {
					name: name.clone(),
					local_name: local_name.clone(),
					ty: Some(quote! { #this_crate ::_reexports::syn::Generics }),
					init: quote! { item.generics.clone() },
					parse_branch: None,
					finish: quote!{#local_name},
				}),
				_ => Err(syn::Error::new(
					span.clone(),
					"Can't use generics parameter type for anything other than an item",
				))
			},
			ParamType::Type => match target {
				ParamTarget::Field => Ok(ParamImpls {
					name: name.clone(),
					local_name: local_name.clone(),
					ty: Some(quote! { #this_crate ::_reexports::syn::Type }),
					init: quote! { item.ty.clone() },
					parse_branch: None,
					finish: quote!{#local_name},
				}),
				_ => Err(syn::Error::new(
					span.clone(),
					"Can't use type parameter type for anything other than a field",
				))
			},
			ParamType::Variants { args_ty } => match target {
				ParamTarget::Item { ty_enum: true, .. } => Ok(ParamImpls {
					name: name.clone(),
					local_name: local_name.clone(),
					ty: Some(quote! { ::std::vec::Vec<#args_ty> }),
					init: quote! { match item.data {
						#this_crate::_reexports::syn::Data::Enum(ref d_enum) => d_enum.variants
							.iter()
							.map(|variant| #args_ty::parse_params(variant))
							.collect::<::std::result::Result<::std::vec::Vec<#args_ty>, _>>()?,
						_ => ::std::vec::Vec::new(),
					}},
					parse_branch: None,
					finish: quote!{#local_name},
				}),
				_ => Err(syn::Error::new(
					span.clone(),
					"Can't use variants parameter type for anything other than an item that incldues enums",
				))
			},
			ParamType::Fields { args_ty } => match target {
				ParamTarget::Item { ty_struct: true, .. } |
				ParamTarget::Item { ty_union: true, .. } => Ok(ParamImpls {
					name: name.clone(),
					local_name: local_name.clone(),
					ty: Some(quote! { ::std::vec::Vec<#args_ty> }),
					init: quote! { match item.data {
						#this_crate::_reexports::syn::Data::Union(ref d) => d.fields.named
							.iter()
							.map(|fields| #args_ty::parse_params(fields))
							.collect::<::std::result::Result<::std::vec::Vec<#args_ty>, _>>()?,
						#this_crate::_reexports::syn::Data::Struct(ref d) => d.fields
							.iter()
							.map(|fields| #args_ty::parse_params(fields))
							.collect::<::std::result::Result<::std::vec::Vec<#args_ty>, _>>()?,
						#this_crate::_reexports::syn::Data::Enum(_) => ::std::vec::Vec::new(),
					}},
					parse_branch: None,
					finish: quote!{#local_name},
				}),
				ParamTarget::Variant => Ok(ParamImpls {
					name: name.clone(),
					local_name: local_name.clone(),
					ty: Some(quote! { ::std::vec::Vec<#args_ty> }),
					init: quote! { item
						.fields
						.iter()
						.map(|fields| #args_ty::parse_params(fields))
						.collect::<::std::result::Result<::std::vec::Vec<#args_ty>, _>>()?
					},
					parse_branch: None,
					finish: quote!{#local_name},
				}),
				_ => Err(syn::Error::new(
					span.clone(),
					"Can't use fields parameter type for anything other than an item that incldues enums",
				))
			},
			ParamType::Boolean => Ok(ParamImpls {
				name: name.clone(),
				local_name: local_name.clone(),
				ty: Some(quote!{ bool }),
				init: quote!{ false },
				parse_branch: Some(quote! {
					#local_name = if stream.peek(#this_crate::_reexports::syn::token::Paren) {
						let content;
						#this_crate::_reexports::syn::parenthesized!(content in stream);
						let litbool = content.parse::<#this_crate::_reexports::syn::LitBool>()?;
						if !content.is_empty() {
							return Err(content.error("expected end of attribute"));
						}
						litbool.value
					} else {
						true
					};
				}),
				finish: quote!{#local_name},
			}),
			ParamType::Required { contents } => Ok(ParamImpls {
				name: name.clone(),
				local_name: local_name.clone(),
				ty: Some(quote!{ ::std::option::Option<#contents> }),
				init: quote!{ ::std::option::Option::None },
				parse_branch: Some(quote! {
					let content;
					#this_crate::_reexports::syn::parenthesized!(content in stream);
					#local_name = Some(content.parse::<#contents>()?);
					if !content.is_empty() {
						return Err(content.error("expected end of attribute"));
					}
				}),
				finish: {
					let errmsg = format!(
						"Missing attribute #[{}({}(...))] is required",
						super_attr_name, name
					);
					quote! {
						#local_name.ok_or_else(|| #this_crate::_reexports::syn::Error::new(
							#this_crate::_reexports::proc_macro2::Span::call_site(),
							#errmsg
						))?
					}
				},
			}),
			ParamType::Optional { contents } => Ok(ParamImpls {
				name: name.clone(),
				local_name: local_name.clone(),
				ty: Some(quote!{ ::std::option::Option<#contents> }),
				init: quote!{ ::std::option::Option::None },
				parse_branch: Some(quote! {
					let content;
					#this_crate::_reexports::syn::parenthesized!(content in stream);
					#local_name = Some(content.parse::<#contents>()?);
					if !content.is_empty() {
						return Err(content.error("expected end of attribute"));
					}
				}),
				finish: quote!{ #local_name },
			}),
			ParamType::Multi { contents, .. } => Ok(ParamImpls {
				name: name.clone(),
				local_name: local_name.clone(),
				ty: Some(quote! {::std::vec::Vec<#contents>}),
				init: quote! { ::std::vec::Vec;:<#contents>::new() },
				parse_branch: Some(quote! {
					let content;
					#this_crate::_reexports::syn::parenthesized!(content in stream);
					#local_name.push(content.parse::<#contents>()?);
					if !content.is_empty() {
						return Err(content.error("expected end of attribute"));
					}
				}),
				finish: quote!{ #local_name },
			}),
			ParamType::Skip => Ok(ParamImpls {
				name: name.clone(),
				local_name: local_name.clone(),
				ty: None,
				init: quote! {::std::default::Default::default()},
				parse_branch: None,
				finish: quote!{ #local_name },
			}),
		}
	}
}

#[non_exhaustive]
pub(crate) enum ParamType {
	/// Don't parse; fill with item/variant/field ident
	Ident,
	/// Don't parse; fill with item generics
	Generics,
	/// Don't parse; fill with field type
	Type,

	/// Parse attribute on variants.
	///
	/// Empty on non-enums.
	Variants { args_ty: syn::Type },
	/// Parse attributes on fields.
	///
	/// Empty on enums.
	Fields { args_ty: syn::Type },

	/// Boolean attribute, either provided or not
	Boolean,

	/// Required attribute
	Required { contents: syn::Type },
	/// Optional attribute
	Optional { contents: syn::Type },
	/// Attribute that can be specified multiple times
	Multi {
		contents: syn::Type,
		min_args: usize,
		max_args: usize,
	},

	/// Don't actually parse field as an attribute; just
	/// fill it in with default
	Skip,
}

/// What the attribute should be specified on
#[non_exhaustive]
pub(crate) enum ParamTarget {
	/// Top-level
	Item {
		ty_struct: bool,
		ty_enum: bool,
		ty_union: bool,
	},
	/// Enum Variant
	Variant,
	/// Field
	Field,
}
impl ParamTarget {
	fn arg_ty(&self) -> TokenStream {
		match self {
			ParamTarget::Item { .. } => quote! { DeriveInput },
			ParamTarget::Variant => quote! { Variant },
			ParamTarget::Field => quote! { Field },
		}
	}

	fn check(&self, this_crate: &TokenStream, super_attr_name: &str) -> TokenStream {
		match self {
			ParamTarget::Item {
				ty_struct,
				ty_enum,
				ty_union,
			} => {
				let err_msg = format!(
					"#[{}] only allowed on [{}{}{}]",
					super_attr_name,
					if *ty_struct { "struct," } else { "" },
					if *ty_enum { "enum," } else { "" },
					if *ty_union { "union," } else { "" },
				);

				let tree_struct = if !*ty_struct {
					quote! { return ::std::result::Result::Err(#this_crate::_reexports::syn::Error::new(#this_crate::_reexports::proc_macro2::Span::call_site(), #err_msg)); }
				} else {
					quote! {}
				};
				let tree_enum = if !*ty_enum {
					quote! { return ::std::result::Result::Err(#this_crate::_reexports::syn::Error::new(#this_crate::_reexports::proc_macro2::Span::call_site(), #err_msg)); }
				} else {
					quote! {}
				};
				let tree_union = if !*ty_union {
					quote! { return ::std::result::Result::Err(#this_crate::_reexports::syn::Error::new(#this_crate::_reexports::proc_macro2::Span::call_site(), #err_msg)); }
				} else {
					quote! {}
				};

				quote! {match item.data {
					#this_crate::_reexports::syn::Data::Struct(_) => { #tree_struct },
					#this_crate::_reexports::syn::Data::Enum(_) => { #tree_enum },
					#this_crate::_reexports::syn::Data::Union(_) => { #tree_union },
				}}
			}
			ParamTarget::Variant => quote! {},
			ParamTarget::Field => quote! {},
		}
	}
}

pub(crate) struct ParamImpls {
	pub name: syn::Ident,
	pub local_name: syn::Ident,
	pub ty: Option<TokenStream>,
	pub init: TokenStream,
	pub parse_branch: Option<TokenStream>,
	pub finish: TokenStream,
}

pub(crate) struct AttributesDef {
	pub name: syn::Ident,
	pub super_attr_name: String,
	pub fields: Vec<FieldAttrs>,
	pub target: ParamTarget,

	pub this_crate: TokenStream,
}
impl AttributesDef {
	pub fn module_ident(&self) -> syn::Ident {
		format_ident!("_col_traits_derive_params_{}", self.name)
	}

	pub fn implementation(&self) -> Result<TokenStream, syn::Error> {
		let name = &self.name;
		let super_attr_name = &self.super_attr_name;
		let this_crate = &self.this_crate;
		let module_ident = self.module_ident();
		let arg_ty = self.target.arg_ty();

		let impls = self
			.fields
			.iter()
			.map(|field| {
				field.implementation(&self.this_crate, &self.super_attr_name, &self.target)
			})
			.collect::<Result<Vec<_>, _>>()?;

		let token_decls = impls
			.iter()
			.filter(|im| im.parse_branch.is_some())
			.map(|im| &im.name)
			.map(|ident| quote! { #this_crate::_reexports::syn::custom_keyword!(#ident); })
			.collect::<TokenStream>();

		let check = self.target.check(&self.this_crate, &self.super_attr_name);

		let inits = impls
			.iter()
			.map(|implem| {
				let local_name = &implem.local_name;
				let init = &implem.init;
				let ty_toks = if let Some(ty) = implem.ty.as_ref() {
					quote! {: #ty}
				} else {
					quote! {}
				};

				quote! { let mut #local_name #ty_toks = #init; }
			})
			.collect::<TokenStream>();

		let lh_branches = impls
			.iter()
			.filter(|implem| implem.parse_branch.is_some())
			.enumerate()
			.map(|(i, implem)| {
				let else_tok = if i == 0 {
					quote! {}
				} else {
					quote! {else}
				};
				let kw_ident = &implem.name;
				let parse_branch = implem.parse_branch.as_ref().unwrap();

				quote! {
					#else_tok if lh.peek(#module_ident :: #kw_ident) {
						stream.parse::<#module_ident :: #kw_ident>()?;
						#parse_branch
					}
				}
			})
			.collect::<TokenStream>();

		let struct_create = impls
			.iter()
			.map(|implem| {
				let field_name = &implem.name;
				let finish = &implem.finish;
				quote! {#field_name : #finish, }
			})
			.collect::<TokenStream>();

		Ok(quote! {
			#[allow(non_snake_case)]
			mod #module_ident {
				#token_decls
			}
			#[allow(unused_mut)]
			impl #name {
				pub fn parse_params(item: &#this_crate::_reexports::syn::#arg_ty) -> Result<Self, #this_crate::_reexports::syn::Error> {
					#check

					#inits
					for attr in item.attrs.iter() {
						if !attr.path.is_ident(#super_attr_name) {
							continue;
						}

						attr.parse_args_with(|stream: #this_crate::_reexports::syn::parse::ParseStream| {
							while !stream.is_empty() {
								let lh = stream.lookahead1();
								#lh_branches
								else {
									return Err(lh.error());
								}

								if !stream.is_empty() {
									stream.parse::<#this_crate ::_reexports::syn::Token!(,)>()?;
								}
							}
							Ok(())
						})?
					}

					Ok(#name {
						#struct_create
					})
				}
			}
		})
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_item_attrs() {
		let params = ItemAttrs::parse_params(&syn::parse_quote! {
			#[derive_params(super_attr(foo), target_struct)]
			struct Foo {
			}
		})
		.unwrap();

		assert!(params.target_struct);
		assert_eq!(params.super_attr, "foo");

		params.check().unwrap();
	}

	/*
	#[test]
	fn test_field_attrs() {

		let field = (|s: syn::parse::ParseStream| -> Result<syn::Field, syn::Error> { s.call(syn::Field::parse_named) }).parse2(quote::quote!{
			#[param_derive(ident)]
			ident: syn::Ident
		}).unwrap();

		let params = FieldAttrs::parse_params(&field).unwrap();

		assert!(params.ident);
		assert_eq!(params.min_args, None);
	}
	*/
}
