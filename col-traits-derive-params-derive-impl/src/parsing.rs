use proc_macro2::TokenStream;

use crate::{
	attrs::*,
	model::*,
};

pub(crate) fn parse(
	di: &syn::DeriveInput,
	this_crate: TokenStream,
) -> Result<AttributesDef, syn::Error> {
	let item_attrs = ItemAttrs::parse_params(di)?;
	item_attrs.check()?;

	let target = item_attrs.target();

	let super_attr_name = item_attrs.super_attr.to_string();

	Ok(AttributesDef {
		name: di.ident.clone(),
		super_attr_name,
		fields: item_attrs.fields,
		target,
		this_crate: item_attrs
			.internal_use_only_crate_root
			.clone()
			.unwrap_or(this_crate),
	})
}
