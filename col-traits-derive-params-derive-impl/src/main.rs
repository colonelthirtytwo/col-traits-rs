use proc_macro2::TokenStream;
use quote::{
	quote,
	ToTokens,
};
use std::{
	io::Read,
	str::FromStr,
};
use syn::parse::Parser;

use col_traits_derive_params_derive_impl::col_derive_params_impl;

fn main() {
	if let Err(err) = real_main() {
		eprintln!("{:?}", err);
		std::process::exit(1);
	}
}

fn real_main() -> Result<(), Box<dyn std::error::Error>> {
	let mut stdin = String::new();
	std::io::stdin().lock().read_to_string(&mut stdin)?;
	let ts = TokenStream::from_str(&stdin).map_err(|e| format!("{:?}", e))?;

	let file = syn::parse2::<syn::File>(ts)?;
	for item in file.items.into_iter() {
		match item {
			syn::Item::Struct(mut struc) => {
				map_fake_attrs(&mut struc.attrs);
				for field in struc.fields.iter_mut() {
					map_fake_attrs(&mut field.attrs);
				}
				let ts = struc.into_token_stream();
				let out = col_derive_params_impl(ts, quote! {crate})?;
				println!("{}", out);
			}
			_ => {}
		}
	}
	Ok(())
}

fn map_fake_attrs(attrs: &mut Vec<syn::Attribute>) {
	for attr in attrs.iter_mut() {
		let new_attr = if let Ok(syn::Meta::List(ref meta)) = attr.parse_meta() {
			if !meta.path.is_ident("cfg_attr") {
				continue;
			}
			if meta.nested.len() != 2 {
				continue;
			}

			if let syn::NestedMeta::Meta(syn::Meta::Path(ref p)) = meta.nested[0] {
				if !p.is_ident("COL_DERIVE_FAKE") {
					continue;
				}
			} else {
				continue;
			}

			if let syn::NestedMeta::Meta(ref inner_meta) = meta.nested[1] {
				syn::Attribute::parse_outer
					.parse2(quote! {
						#[#inner_meta]
					})
					.unwrap()
					.into_iter()
					.next()
					.unwrap()
			} else {
				continue;
			}
		} else {
			continue;
		};
		*attr = new_attr;
	}
}
