#[derive(Clone)]
#[cfg_attr(
	COL_DERIVE_FAKE,
	derive_params(super_attr(derive_params), target_struct)
)]
pub(crate) struct ItemAttrs {
	#[cfg_attr(COL_DERIVE_FAKE, derive_params(fields))]
	pub fields: Vec<FieldAttrs>,

	pub target_struct: bool,
	pub target_enum: bool,
	pub target_union: bool,
	pub target_variant: bool,
	pub target_field: bool,

	pub super_attr: syn::Ident,

	pub internal_use_only_crate_root: Option<proc_macro2::TokenStream>,
}

#[derive(Clone)]
#[cfg_attr(
	COL_DERIVE_FAKE,
	derive_params(super_attr(derive_params), target_field)
)]
pub(crate) struct FieldAttrs {
	#[cfg_attr(COL_DERIVE_FAKE, derive_params(ident))]
	pub field_ident: Option<syn::Ident>,
	#[cfg_attr(COL_DERIVE_FAKE, derive_params(ty))]
	pub field_ty: syn::Type,

	pub ident: bool,
	pub generics: bool,
	pub ty: bool,
	pub variants: bool,
	pub fields: bool,

	pub skip: bool,
	// TODO:
	//pub min_args: Option<syn::LitInt>,
	//pub max_args: Option<syn::LitInt>,
}

include!("attrs.generated.rs");
