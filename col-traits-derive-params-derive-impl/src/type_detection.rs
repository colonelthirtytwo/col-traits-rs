/// Checks if the type is an Option, and returns the wrapped type
/// if so.
pub(crate) fn is_option(ty: &syn::Type) -> Option<&syn::Type> {
	if let syn::Type::Path(ty_path) = ty {
		if ty_path.qself.is_some() {
			return None;
		}

		let is_option_path = (ty_path.path.segments.len() == 1
			&& ty_path.path.segments[0].ident == "Option")
			|| (ty_path.path.segments.len() == 3
				&& ty_path.path.segments[0].ident == "std"
				&& ty_path.path.segments[1].ident == "option"
				&& ty_path.path.segments[2].ident == "Option");

		if is_option_path {
			if let syn::PathArguments::AngleBracketed(ref args) =
				ty_path.path.segments.last().unwrap().arguments
			{
				if args.args.len() != 1 {
					return None;
				}
				if let syn::GenericArgument::Type(ref ty) = args.args[0] {
					return Some(ty);
				}
			} else {
				return None;
			}
		}
	}
	None
}

/// Checks if the type is a Vec, and returns the wrapped type
/// if so.
pub(crate) fn is_vec(ty: &syn::Type) -> Option<&syn::Type> {
	if let syn::Type::Path(ty_path) = ty {
		if ty_path.qself.is_some() {
			return None;
		}

		let is_option_path = (ty_path.path.segments.len() == 1
			&& ty_path.path.segments[0].ident == "Vec")
			|| (ty_path.path.segments.len() == 3
				&& ty_path.path.segments[0].ident == "std"
				&& ty_path.path.segments[1].ident == "vec"
				&& ty_path.path.segments[2].ident == "Vec");

		if is_option_path {
			if let syn::PathArguments::AngleBracketed(ref args) =
				ty_path.path.segments.last().unwrap().arguments
			{
				if args.args.len() != 1 {
					return None;
				}
				if let syn::GenericArgument::Type(ref ty) = args.args[0] {
					return Some(ty);
				}
			} else {
				return None;
			}
		}
	}
	None
}

/// Checks if the type is `$desired_name`.
pub(crate) fn is_prim_typ(ty: &syn::Type, desired_name: &str) -> bool {
	if let syn::Type::Path(ty_path) = ty {
		if ty_path.qself.is_some() {
			return false;
		}

		let is_path =
			ty_path.path.segments.len() == 1 && ty_path.path.segments[0].ident == desired_name;
		return is_path;
	}
	false
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_is_option() {
		assert_eq!(
			is_option(&syn::parse_quote! { Option<f32> }),
			Some(&syn::parse_quote! { f32 })
		);
		assert_eq!(
			is_option(&syn::parse_quote! { std::option::Option<f32> }),
			Some(&syn::parse_quote! { f32 })
		);
		assert_eq!(
			is_option(&syn::parse_quote! { ::std::option::Option<f32> }),
			Some(&syn::parse_quote! { f32 })
		);
		assert_eq!(is_option(&syn::parse_quote! { f32 }), None);
		assert_eq!(
			is_option(&syn::parse_quote! { other::module::Option<f64> }),
			None
		);
	}

	#[test]
	fn test_is_vec() {
		assert_eq!(
			is_vec(&syn::parse_quote! { Vec<f32> }),
			Some(&syn::parse_quote! { f32 })
		);
		assert_eq!(
			is_vec(&syn::parse_quote! { std::vec::Vec<f32> }),
			Some(&syn::parse_quote! { f32 })
		);
		assert_eq!(
			is_vec(&syn::parse_quote! { ::std::vec::Vec<f32> }),
			Some(&syn::parse_quote! { f32 })
		);
		assert_eq!(is_vec(&syn::parse_quote! { f32 }), None);
		assert_eq!(is_vec(&syn::parse_quote! { other::module::Vec<f64> }), None);
	}
}
