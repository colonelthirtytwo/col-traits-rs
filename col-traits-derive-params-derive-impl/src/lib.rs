pub(crate) mod attrs;
pub(crate) mod model;
pub(crate) mod parsing;
pub(crate) mod type_detection;

use proc_macro2::TokenStream;
use std::iter::FromIterator;

pub fn col_derive_params_impl(
	ts: TokenStream,
	this_crate: TokenStream,
) -> Result<TokenStream, syn::Error> {
	let def = parsing::parse(&syn::parse2(ts)?, this_crate)?;
	def.implementation()
}

/// Combines an iterator of errors
pub(crate) fn combine_errors<I: IntoIterator<Item = syn::Error>>(
	into_iter: I,
) -> Result<(), syn::Error> {
	collect_combine_errors::<(), _, _>(into_iter.into_iter().map(Err))
}

pub(crate) fn collect_combine_errors<O, I, V>(into_iter: I) -> Result<O, syn::Error>
where
	I: IntoIterator<Item = Result<V, syn::Error>>,
	O: FromIterator<V>,
{
	let mut combined_error: Option<syn::Error> = None;
	let out = into_iter
		.into_iter()
		.scan((), |&mut (), item| {
			if let Some(ref mut err) = combined_error {
				if let Err(e) = item {
					err.combine(e);
				}
				None
			} else {
				match item {
					Ok(v) => Some(v),
					Err(e) => {
						combined_error = Some(e);
						None
					}
				}
			}
		})
		.collect::<O>();

	if let Some(e) = combined_error {
		Err(e)
	} else {
		Ok(out)
	}
}

// Bootstrapping stuff

pub(crate) mod _reexports {
	pub use ::proc_macro2;
	pub use ::syn;
}
