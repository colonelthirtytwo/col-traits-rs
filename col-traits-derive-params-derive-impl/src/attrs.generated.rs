#[allow(non_snake_case)]
mod _col_traits_derive_params_ItemAttrs {
	crate::_reexports::syn::custom_keyword!(target_struct);
	crate::_reexports::syn::custom_keyword!(target_enum);
	crate::_reexports::syn::custom_keyword!(target_union);
	crate::_reexports::syn::custom_keyword!(target_variant);
	crate::_reexports::syn::custom_keyword!(target_field);
	crate::_reexports::syn::custom_keyword!(super_attr);
	crate::_reexports::syn::custom_keyword!(internal_use_only_crate_root);
}
#[allow(unused_mut)]
impl ItemAttrs {
	pub fn parse_params(
		item: &crate::_reexports::syn::DeriveInput,
	) -> Result<Self, crate::_reexports::syn::Error> {
		match item.data {
			crate::_reexports::syn::Data::Struct(_) => {}
			crate::_reexports::syn::Data::Enum(_) => {
				return ::std::result::Result::Err(crate::_reexports::syn::Error::new(
					crate::_reexports::proc_macro2::Span::call_site(),
					"#[derive_params] only allowed on [struct,]",
				));
			}
			crate::_reexports::syn::Data::Union(_) => {
				return ::std::result::Result::Err(crate::_reexports::syn::Error::new(
					crate::_reexports::proc_macro2::Span::call_site(),
					"#[derive_params] only allowed on [struct,]",
				));
			}
		}
		let mut arg_fields: ::std::vec::Vec<FieldAttrs> = match item.data {
			crate::_reexports::syn::Data::Union(ref d) => d
				.fields
				.named
				.iter()
				.map(|fields| FieldAttrs::parse_params(fields))
				.collect::<::std::result::Result<::std::vec::Vec<FieldAttrs>, _>>()?,
			crate::_reexports::syn::Data::Struct(ref d) => d
				.fields
				.iter()
				.map(|fields| FieldAttrs::parse_params(fields))
				.collect::<::std::result::Result<::std::vec::Vec<FieldAttrs>, _>>()?,
			crate::_reexports::syn::Data::Enum(_) => ::std::vec::Vec::new(),
		};
		let mut arg_target_struct: bool = false;
		let mut arg_target_enum: bool = false;
		let mut arg_target_union: bool = false;
		let mut arg_target_variant: bool = false;
		let mut arg_target_field: bool = false;
		let mut arg_super_attr: ::std::option::Option<syn::Ident> = ::std::option::Option::None;
		let mut arg_internal_use_only_crate_root: ::std::option::Option<proc_macro2::TokenStream> =
			::std::option::Option::None;
		for attr in item.attrs.iter() {
			if !attr.path.is_ident("derive_params") {
				continue;
			}
			attr.parse_args_with(|stream: crate::_reexports::syn::parse::ParseStream| {
				while !stream.is_empty() {
					let lh = stream.lookahead1();
					if lh.peek(_col_traits_derive_params_ItemAttrs::target_struct) {
						stream.parse::<_col_traits_derive_params_ItemAttrs::target_struct>()?;
						arg_target_struct = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_ItemAttrs::target_enum) {
						stream.parse::<_col_traits_derive_params_ItemAttrs::target_enum>()?;
						arg_target_enum = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_ItemAttrs::target_union) {
						stream.parse::<_col_traits_derive_params_ItemAttrs::target_union>()?;
						arg_target_union = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_ItemAttrs::target_variant) {
						stream.parse::<_col_traits_derive_params_ItemAttrs::target_variant>()?;
						arg_target_variant = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_ItemAttrs::target_field) {
						stream.parse::<_col_traits_derive_params_ItemAttrs::target_field>()?;
						arg_target_field = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_ItemAttrs::super_attr) {
						stream.parse::<_col_traits_derive_params_ItemAttrs::super_attr>()?;
						let content;
						crate :: _reexports :: syn :: parenthesized ! ( content in stream );
						arg_super_attr = Some(content.parse::<syn::Ident>()?);
						if !content.is_empty() {
							return Err(content.error("expected end of attribute"));
						}
					} else if lh
						.peek(_col_traits_derive_params_ItemAttrs::internal_use_only_crate_root)
					{
						stream
							.parse::<_col_traits_derive_params_ItemAttrs::internal_use_only_crate_root>(
							)?;
						let content;
						crate :: _reexports :: syn :: parenthesized ! ( content in stream );
						arg_internal_use_only_crate_root =
							Some(content.parse::<proc_macro2::TokenStream>()?);
						if !content.is_empty() {
							return Err(content.error("expected end of attribute"));
						}
					} else {
						return Err(lh.error());
					}
					if !stream.is_empty() {
						stream.parse::<crate :: _reexports :: syn :: Token ! ( , )>()?;
					}
				}
				Ok(())
			})?
		}
		Ok(ItemAttrs {
			fields: arg_fields,
			target_struct: arg_target_struct,
			target_enum: arg_target_enum,
			target_union: arg_target_union,
			target_variant: arg_target_variant,
			target_field: arg_target_field,
			super_attr: arg_super_attr.ok_or_else(|| {
				crate::_reexports::syn::Error::new(
					crate::_reexports::proc_macro2::Span::call_site(),
					"Missing attribute #[derive_params(super_attr(...))] is required",
				)
			})?,
			internal_use_only_crate_root: arg_internal_use_only_crate_root,
		})
	}
}
#[allow(non_snake_case)]
mod _col_traits_derive_params_FieldAttrs {
	crate::_reexports::syn::custom_keyword!(ident);
	crate::_reexports::syn::custom_keyword!(generics);
	crate::_reexports::syn::custom_keyword!(ty);
	crate::_reexports::syn::custom_keyword!(variants);
	crate::_reexports::syn::custom_keyword!(fields);
	crate::_reexports::syn::custom_keyword!(skip);
}
#[allow(unused_mut)]
impl FieldAttrs {
	pub fn parse_params(
		item: &crate::_reexports::syn::Field,
	) -> Result<Self, crate::_reexports::syn::Error> {
		let mut arg_field_ident: ::std::option::Option<crate::_reexports::syn::Ident> =
			item.ident.clone();
		let mut arg_field_ty: crate::_reexports::syn::Type = item.ty.clone();
		let mut arg_ident: bool = false;
		let mut arg_generics: bool = false;
		let mut arg_ty: bool = false;
		let mut arg_variants: bool = false;
		let mut arg_fields: bool = false;
		let mut arg_skip: bool = false;
		for attr in item.attrs.iter() {
			if !attr.path.is_ident("derive_params") {
				continue;
			}
			attr.parse_args_with(|stream: crate::_reexports::syn::parse::ParseStream| {
				while !stream.is_empty() {
					let lh = stream.lookahead1();
					if lh.peek(_col_traits_derive_params_FieldAttrs::ident) {
						stream.parse::<_col_traits_derive_params_FieldAttrs::ident>()?;
						arg_ident = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_FieldAttrs::generics) {
						stream.parse::<_col_traits_derive_params_FieldAttrs::generics>()?;
						arg_generics = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_FieldAttrs::ty) {
						stream.parse::<_col_traits_derive_params_FieldAttrs::ty>()?;
						arg_ty = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_FieldAttrs::variants) {
						stream.parse::<_col_traits_derive_params_FieldAttrs::variants>()?;
						arg_variants = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_FieldAttrs::fields) {
						stream.parse::<_col_traits_derive_params_FieldAttrs::fields>()?;
						arg_fields = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else if lh.peek(_col_traits_derive_params_FieldAttrs::skip) {
						stream.parse::<_col_traits_derive_params_FieldAttrs::skip>()?;
						arg_skip = if stream.peek(crate::_reexports::syn::token::Paren) {
							let content;
							crate :: _reexports :: syn :: parenthesized ! ( content in stream );
							let litbool = content.parse::<crate::_reexports::syn::LitBool>()?;
							if !content.is_empty() {
								return Err(content.error("expected end of attribute"));
							}
							litbool.value
						} else {
							true
						};
					} else {
						return Err(lh.error());
					}
					if !stream.is_empty() {
						stream.parse::<crate :: _reexports :: syn :: Token ! ( , )>()?;
					}
				}
				Ok(())
			})?
		}
		Ok(FieldAttrs {
			field_ident: arg_field_ident,
			field_ty: arg_field_ty,
			ident: arg_ident,
			generics: arg_generics,
			ty: arg_ty,
			variants: arg_variants,
			fields: arg_fields,
			skip: arg_skip,
		})
	}
}
